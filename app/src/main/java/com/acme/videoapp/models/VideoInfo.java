package com.acme.videoapp.models;

import org.joda.time.DateTime;

import java.io.Serializable;

public class VideoInfo implements Serializable {
    private int id;
    private String title;
    private String description;
    private DateTime dateTime;
    private String fileName;


    public VideoInfo(int id, String title, String description, DateTime dateTime, String fileName) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
