package com.acme.videoapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import com.acme.videoapp.adapters.VideoInfoAdapter;
import com.acme.videoapp.models.VideoInfo;
import com.acme.videoapp.rest.APIVideoInfoClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoFeedActivity extends AppCompatActivity {
    public static final int REQUEST_EXT_STORAGE_PERM = 1;
    public static boolean extStoragePermGranted = false;
    private List<VideoInfo> videoInfoList;
    private ListView videoInfoListView;
    private Parcelable state;

    ProgressDialog loading;

    private static final String DESC = "this is a description1 and this is just mumble jumble. Lets keep going on and on and on.this is a description1 and this is just mumble jumble. Lets keep going on and on and on.";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_feed);

        videoInfoListView = (ListView) findViewById(R.id.video_info_listview);


        askForPermission();
        getVideoInfoList();
    }

    @Override
    public void onPause() {
        // TODO: could be improved by the saving just the video reference which was last visible instead of saving the listview
        state = videoInfoListView.onSaveInstanceState();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null != state) {
            Log.d("VideoFeed", Integer.toString(state.describeContents()));
            videoInfoListView.onRestoreInstanceState(state);
        }
    }

    private void getVideoInfoList() {
        loading = ProgressDialog.show(this, "Loading...", "Please wait while loading data.", true);

        Call<List<VideoInfo>> call = APIVideoInfoClient.get().getVideoInfoResults();

        call.enqueue(new Callback<List<VideoInfo>>() {
            @Override
            public void onResponse(Call<List<VideoInfo>> call, Response<List<VideoInfo>> response) {
                Log.d("API", "Successfully response fetched");

                loading.dismiss();

                videoInfoList = response.body();

                if (videoInfoList.size() > 0) {
                    showList();
                } else {
                    Log.d("APIPlug", "No item found");

                }
            }

            @Override
            public void onFailure(Call<List<VideoInfo>> call, Throwable t) {
                Log.d("API", "Error Occured: " + t.getMessage());
                loading.dismiss();
                // For testing on device, connecting to localhost API will take some time.
                // So, if not from API then the data is mocked
                // int id, String title, String description, DateTime dateTime, String fileName
                videoInfoList = new ArrayList<VideoInfo>();
                videoInfoList.add(new VideoInfo(1, "title1", DESC, DateTime.now(), "video1"));
                videoInfoList.add(new VideoInfo(2, "title2", DESC, DateTime.now(), "video2"));
                videoInfoList.add(new VideoInfo(3, "title3", DESC, DateTime.now(), "video3"));
                videoInfoList.add(new VideoInfo(4, "title4", DESC, DateTime.now(), "video4"));
                videoInfoList.add(new VideoInfo(5, "title5", DESC, DateTime.now(), "video5"));
                showList();
            }
        });
    }

    private void showList() {
        Log.d("API", "Show video info list");

        VideoInfoAdapter adapter = new VideoInfoAdapter(this, videoInfoList, extStoragePermGranted);
        videoInfoListView.setAdapter(adapter);

        videoInfoListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d("Itemclick" ,"Video clicked" );
                VideoInfo videoInfo = (VideoInfo) adapterView.getItemAtPosition(position);
                Intent intent = new Intent(view.getContext(), VideoDetailsActivity.class);
                String s = (new Gson().toJson(videoInfo));
                intent.putExtra("video_info", s);
                startActivity(intent);

                //Intent intent = new Intent(view.getContext(), VideoPlayerActivity.class);
                //String s = (new Gson().toJson(videoInfo));
                //intent.putExtra("video_info", s);
                //startActivity(intent);
            }
        });
        
        videoInfoListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Log.d("FeedActivity", "i" + Integer.toString(i) + "i1" + Integer.toString(i1) + "i2" + Integer.toString(i2));
            }
        });
    }

    public void onAddVideo(View view) {
        Intent intent = new Intent(view.getContext(), AddVideoActivity.class);
        startActivity(intent);
    }

    public void askForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, this.REQUEST_EXT_STORAGE_PERM);
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == this.REQUEST_EXT_STORAGE_PERM) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                extStoragePermGranted = true;
            } else {
                extStoragePermGranted = false;
            }
        }
    }
}
