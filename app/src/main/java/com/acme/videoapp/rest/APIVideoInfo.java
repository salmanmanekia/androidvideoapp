package com.acme.videoapp.rest;

import java.util.List;

import com.acme.videoapp.models.VideoInfo;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIVideoInfo {

    @GET("video_info")
    Call<List<VideoInfo>> getVideoInfoResults();

    @POST("video_info")
    Call<VideoInfo> createVideoInfo(@Body VideoInfo videoInfo);
}
