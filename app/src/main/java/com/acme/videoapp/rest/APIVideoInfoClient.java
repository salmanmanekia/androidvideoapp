package com.acme.videoapp.rest;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIVideoInfoClient {
    private static APIVideoInfo REST_CLIENT;
    private static final String API_URL = "http://10.0.2.2:8000/api/";

    static {
        setupRestClient();
    }

    private APIVideoInfoClient() {
    }

    public static APIVideoInfo get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        REST_CLIENT = retrofit.create(APIVideoInfo.class);
    }
}
