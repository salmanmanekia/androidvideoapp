package com.acme.videoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AddVideoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_video);
    }

    public void onAddVideoClick(View view) {
        Intent intent = new Intent(view.getContext(), VideoFeedActivity.class);
        startActivity(intent);
    }
}
