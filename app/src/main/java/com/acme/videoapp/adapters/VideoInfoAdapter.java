package com.acme.videoapp.adapters;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Field;
import java.util.List;

import com.acme.videoapp.BuildConfig;
import com.acme.videoapp.R;
import com.acme.videoapp.models.VideoInfo;

public class VideoInfoAdapter extends ArrayAdapter<VideoInfo> {

    private final boolean extStoragePermGranted;

    private static class ViewHolder {
        public TextView textTitle;
        public TextView textDescription;
        public TextView textDateTime;
        public TextureView videoView;
    }

    public VideoInfoAdapter(Context context, List<VideoInfo> videoInfoList, boolean extStoragePermGranted) {
        super(context, 0, videoInfoList);
        this.extStoragePermGranted = extStoragePermGranted;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        VideoInfo videoInfo = getItem(position);
        ViewHolder viewHolder;
        View rowView = convertView;
        if (rowView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            // configure view holder
            rowView = inflater.inflate(R.layout.video_info_row_layout, parent, false);
            viewHolder.textTitle = (TextView) rowView.findViewById(R.id.title_view);
            viewHolder.textDescription = (TextView) rowView.findViewById(R.id.description_view);
            viewHolder.textDateTime = (TextView) rowView.findViewById(R.id.datetime_view);
            viewHolder.videoView = (TextureView) rowView.findViewById(R.id.video_preview);

            rowView.setTag(viewHolder);


        }

        viewHolder = (ViewHolder) rowView.getTag();
       /* if (viewHolder.videoView != null) {
            Log.d("VideoInfo", "Release TextureView");
            SurfaceTexture st = viewHolder.videoView.getSurfaceTexture();
                if (st != null) {
                    Log.d("VideoInfo", "Release SurfaceTexture");
                    st.release();

                }

        }*/
        try {
            Object rawObject = Class.forName("com.acme.videoapp.R$raw").newInstance();
            Field field = rawObject.getClass().getField(videoInfo.getFileName());
            int videoRId = (int) field.get(rawObject);

            Uri videoURI = Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/"
                    + videoRId);
            Log.d("VInfoAdpt getView()", videoURI.toString());

            viewHolder.videoView.setId(viewHolder.videoView.generateViewId());
            viewHolder.videoView.setSurfaceTextureListener(new VideoSurfaceTextureListener(videoURI));


        } catch (Exception e) {
            e.printStackTrace();
        }

        viewHolder.textTitle.setText(videoInfo.getTitle());
        viewHolder.textDescription.setText(videoInfo.getDescription());
        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm dd-MM-yyyy");
        viewHolder.textDateTime.setText(fmt.print(videoInfo.getDateTime()));

        return rowView;

    }


    public class VideoSurfaceTextureListener implements TextureView.SurfaceTextureListener {

        MediaPlayer mp;
        Uri videoURI;

        public VideoSurfaceTextureListener(Uri videoURI) {
            this.videoURI = videoURI;
        }

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {

            mp = new MediaPlayer();
            Log.d("VideoInfoAdapter","OnSurfaceTextureAvailable");
            Surface s = new Surface(surfaceTexture);


            try {
                Log.d("VInfoAdpt surfaceAvail", videoURI.toString());

                mp.setDataSource(getContext(), videoURI, null);

                mp.prepareAsync();

                mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        Log.d("VideoInfoAdapter","Media is prepared");
                        mediaPlayer.start();
                    }
                });
                mp.setSurface(s);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

            }
            catch (Exception e ) {

            }

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            Log.d("VideoInfoAdapter","OnSurfaceTextureDestroyed");

            if (mp != null) {
                mp.stop();
                mp.release();
                mp = null;
            }

            return false;


        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }

    }
}