package com.acme.videoapp;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.acme.videoapp.models.VideoInfo;
import com.google.gson.Gson;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Field;

public class VideoDetailsActivity
        extends AppCompatActivity
        implements SurfaceHolder.Callback, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener, MediaController.MediaPlayerControl {

    private MediaPlayer mPlayer;
    private SurfaceView mView;
    private SurfaceHolder mHolder;
    private ProgressBar mProgressBar;
    private MediaController mMediaController;
    private Handler mHandler;
    private String mFilePath;
    private Uri mVideoUri;
    private String videoId;
    VideoInfo videoInfo;
    TextView textTitle;
    TextView textDescription;
    TextView textDateTime;
    DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm dd-MM-yyyy");
    Runnable mediaControllerRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {

            setContentView(R.layout.activity_video_details);

            Intent intent = getIntent();
            String videoInfoStr = intent.getStringExtra("video_info");
            videoInfo = new Gson().fromJson(videoInfoStr, VideoInfo.class);

            textTitle = (TextView) findViewById(R.id.title_view);
            textDescription = (TextView) findViewById(R.id.description_view);
            textDateTime = (TextView) findViewById(R.id.datetime_view);

            videoId = videoInfo.getFileName();
            textTitle.setText(videoInfo.getTitle());
            textDateTime.setText(fmt.print(videoInfo.getDateTime()));
            textDescription.setText(videoInfo.getDescription());
        } catch (Exception e) {
            Log.e("VID_DETAIL_ACT","Error in creating the instance out of R.id or getting the video field out of the instance");
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle state) {

        state.putString("video_id", videoId);
        state.putString("video_title", videoInfo.getTitle());
        state.putString("video_desc", videoInfo.getDescription());
        state.putString("video_date", fmt.print(videoInfo.getDateTime()));

    }

    @Override
    public void onRestoreInstanceState(Bundle state) {
        if (state == null) {
            videoId = videoInfo.getFileName();
            textTitle.setText(videoInfo.getTitle());
            textDateTime.setText(fmt.print(videoInfo.getDateTime()));
            textDescription.setText(videoInfo.getDescription());
        } else {
            videoId = state.getString("video_id");
            textTitle.setText(state.getString("video_title"));
            textDateTime.setText(state.getString("video_date"));
            textDescription.setText(state.getString("video_desc"));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPlayer = new MediaPlayer();

        mView = (SurfaceView) findViewById(R.id.video_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mHandler = new Handler();

        mHolder = mView.getHolder();
        mHolder.setFixedSize(640, 480);
        mHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mPlayer.setDisplay(surfaceHolder);
        play();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) { }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        mPlayer.stop();
    }

    public void play()  {
        try {
            Object rawObject = Class.forName("com.acme.videoapp.R$raw").newInstance();
            Field field =rawObject.getClass().getField(videoId);
            int videoRId = (int)field.get(rawObject);

            Log.d("VideoDetailsActivity",
                    "resid taken through reflection "+ Integer.toString(videoRId));

            mVideoUri = Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/" + videoRId);
            mMediaController = new MediaController(this){
                @Override
                public boolean dispatchKeyEvent(KeyEvent event)
                {
                    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK)
                        ((Activity) getContext()).finish();
                    return super.dispatchKeyEvent(event);
                }

            };
            mPlayer.setDataSource(this, mVideoUri, null);
            mPlayer.prepareAsync();
            mPlayer.setOnBufferingUpdateListener(this);
            mPlayer.setOnCompletionListener(this);
            mPlayer.setOnPreparedListener(this);
            mPlayer.setScreenOnWhilePlaying(true);
            mPlayer.setOnVideoSizeChangedListener(this);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mProgressBar.setVisibility(View.GONE);
        mMediaController.setMediaPlayer(this);
        mMediaController.setAnchorView(findViewById(R.id.video_view));
        mMediaController.setEnabled(true);

        mediaControllerRunnable = new Runnable() {
            public void run() {
                mMediaController.show();
            }
        };
        mHandler.post(mediaControllerRunnable);
    }

    @Override
    public void start() {
        mPlayer.start();
    }

    @Override
    public void pause() {
        mPlayer.pause();
    }

    @Override
    public int getDuration() {
        return mPlayer.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return mPlayer.getCurrentPosition();
    }

    @Override
    public void seekTo(int i) {
        mPlayer.seekTo(i);
    }

    @Override
    public boolean isPlaying() {
        return mPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mMediaController.show();
        return false;
    }

}
